const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const { urlencoded, json } = require('body-parser')

const { getAll, createOne, removeOne, updateOne } = require('./server.controllers')
const { model } = require('./server.model')

const app = express()
const port = 8080

app.use(json())
app.use(urlencoded({ extended: true }))
app.use(morgan('dev'))

app.get('/api/news', getAll(model))
app.post('/api/news', createOne(model))
app.delete('/api/news', removeOne(model))
app.put('/api/news', updateOne(model))

module.exports = app

app.listen(port, async () => {
  await mongoose.connect('mongodb://localhost:27017/news')
  console.log(`REST API on localhost ${port}`)
})
