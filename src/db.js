const mongoose = require('mongoose')

// connect to database
const connect = async () => {
  // await mongoose.connect(uri, mongooseOpts)
  await mongoose.connect('mongodb://localhost:27017/news')
}

// disconnect and close the connection
const closeDatabase = async () => {
  await mongoose.connection.dropDatabase()
  await mongoose.connection.close()
}

// clear the database
const clearDatabase = async () => {
  const collections = mongoose.connection.collections
  for (const key in collections) {
    const collection = collections[key]
    await collection.deleteMany()
  }
}

module.exports = { connect, closeDatabase, clearDatabase }
