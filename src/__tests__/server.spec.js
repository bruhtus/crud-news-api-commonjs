const request = require('supertest')
const app = require('../server')

const db = require('../db')

beforeAll(async () => await db.connect())
afterAll(async () => await db.clearDatabase())
afterAll(async () => await db.closeDatabase())

describe('crud routes exist', () => {
  test('can create news', async () => {
    const res = await request(app)
    .post('/api/news')
    .send({ title: 'jakarta banjir', content: 'masih tenggelam' })

    expect(res.statusCode).toBe(201)
    expect(res.body.data).toMatchObject({
      title: 'jakarta banjir',
      content: 'masih tenggelam'
    })
  })

  test('can get news', async () => {
    const res = await request(app)
    .get('/api/news')

    expect(res.statusCode).toBe(201)
    expect(res.body.data).toMatchObject([{
      title: 'jakarta banjir',
      content: 'masih tenggelam'
    }])
  })

  test('can update news', async () => {
    const res = await request(app)
    .put('/api/news')
    .send({ title: 'jakarta banjir', newTitle: 'jakarta tenggelam' })

    expect(res.statusCode).toBe(201)
    expect(res.body.data).toMatchObject({ title: 'jakarta tenggelam' })
  })

  test('can delete news', async () => {
    const res = await request(app)
    .delete('/api/news')
    .send({ title: 'jakarta tenggelam' })

    expect(res.statusCode).toBe(201)
    expect(res.body.data).toMatchObject({ title: 'jakarta tenggelam' })
  })

  test('cannot delete news with title jogja', async () => {
    const res = await request(app)
    .delete('/api/news')
    .send({ title: 'jogja' })

    expect(res.statusCode).toBe(400)
    expect(res.body).toMatchObject({ message: 'cannot be deleted' })
  })
})
