// read all item in database
const getAll = model => async (req, res) => {
  const docs = await model.find()
  return res.status(201).json({ data: docs })
}

// create one item
const createOne = model => async (req, res) => {
  const doc = await model.create(req.body)
  return res.status(201).json({ data: doc })
}

// delete one item based on the title
const removeOne = model => async (req, res) => {
  if (req.body.title === 'jogja') {
   return res.status(400).json({ message: 'cannot be deleted' })
  }
  const removed = await model.findOneAndRemove({
    title: req.body.title
  })
  return res.status(201).json({ data: removed })
}

// update one item based on the title
const updateOne = model => async (req, res) => {
  const updated = await model.findOneAndUpdate(
    { title: req.body.title },
    { title: req.body.newTitle },
    { new: true }
  )
  return res.status(201).json({ data: updated })
}

module.exports = { getAll, createOne, removeOne, updateOne }
