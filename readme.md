## References

- [Example of findOneAndUpdate() in mongoose](https://mongoosejs.com/docs/tutorials/findoneandupdate.html).
- [Unit testing node.js + mongoose using jest](https://javascript.plainenglish.io/unit-testing-node-js-mongoose-using-jest-106a39b8393d).
- [Jest expect documentation](https://jestjs.io/docs/expect).
